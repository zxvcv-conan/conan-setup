Conan Setup Files
=================
This repository contians conan configuration to work with zxvcvs' conan packages.

Installation
============
```
conan config install https://gitlab.com/zxvcv-conan/conan-setup.git
```

More information about ```conan config install``` command: https://docs.conan.io/en/latest/reference/commands/consumer/config.html#conan-config-install
